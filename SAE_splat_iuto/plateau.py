"""module de gestion du plateau de jeu
"""
import const
import case

# dictionnaire permettant d'associer une direction et la position relative
# de la case qui se trouve dans cette direction
INC_DIRECTION = {'N': (-1, 0), 'E': (0, 1), 'S': (1, 0),
                 'O': (0, -1), 'X': (0, 0)}


def get_nb_lignes(plateau):
    """retourne le nombre de lignes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de lignes du plateau
    """
    return plateau["nb_lignes"]


def get_nb_colonnes(plateau):
    """retourne le nombre de colonnes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de colonnes du plateau
    """
    return plateau["nb_colonnes"]


def get_case(plateau, pos):
    """retourne la case qui se trouve à la position pos du plateau

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        dict: La case qui se situe à la position pos du plateau
    """
    return plateau[pos]

def poser_joueur(plateau, joueur, pos):
    """pose un joueur en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int
    """
    la_case = get_case(plateau, pos)
    la_case["joueurs_presents"] = joueur
    plateau[pos] = la_case
    return plateau

def poser_objet(plateau, objet, pos):
    """Pose un objet en position pos sur le plateau. Si cette case contenait déjà
        un objet ce dernier disparait

    Args:
        plateau (dict): le plateau considéré
        objet (int): un entier représentant l'objet. const.AUCUN indique aucun objet
        pos (tuple): une paire (lig,col) de deux int
    """
    la_case = get_case(plateau, pos)
    la_case["objet"] = objet
    plateau[pos] = la_case
    return plateau

def plateau_from_str(la_chaine):
    """Construit un plateau à partir d'une chaine de caractère contenant les informations
        sur le contenu du plateau (voir sujet)

    Args:
        la_chaine (str): la chaine de caractères décrivant le plateau

    Returns:
        dict: le plateau correspondant à la chaine. None si l'opération a échoué
    """
    plateau = {}
    liste_ligne = la_chaine.split("\n")
    ligne_colonne = liste_ligne[0].split(";")
    plateau["nb_lignes"] = int(ligne_colonne[0]) 
    plateau["nb_colonnes"] = int(ligne_colonne[1])
    num_ligne = 0
    num_colonne = 0
    for ligne in liste_ligne :
        if ligne.split(';') != ligne_colonne :
            for valeur in ligne :
                plateau[(num_ligne, num_colonne)] = valeur 
                num_colonne += 1
            num_colonne = 0
            num_ligne += 1
    return plateau

def Plateau(plan):
    """Créer un plateau en respectant le plan donné en paramètre.
        Le plan est une chaine de caractères contenant
            '#' (mur)
            ' ' (couloir non peint)
            une lettre majuscule (un couloir peint par le joueur représenté par la lettre)

    Args:
        plan (str): le plan sous la forme d'une chaine de caractères

    Returns:
        dict: Le plateau correspondant au plan
    """
    plateau = {}
    liste_ligne = plan.split("\n")
    ligne_colonne = liste_ligne[0].split(";")
    liste_ligne.pop(0)
    plateau["nb_lignes"] = int(ligne_colonne[0]) 
    plateau["nb_colonnes"] = int(ligne_colonne[1])
    num_colonne = 0
    for i in range(len(liste_ligne)) :
        if i <= plateau["nb_lignes"]-1 :
            for valeur in liste_ligne[i]:
                if valeur == '#':
                    plateau[(i, num_colonne)] = case.Case(True)
                elif valeur in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                    plateau[(i, num_colonne)] = case.Case(False, valeur)
                elif valeur in 'abcdefghijklmnopqrstuvwxyz' :
                    plateau[(i, num_colonne)] = case.Case(True, valeur)
                elif valeur == ' ':
                    plateau[(i, num_colonne)] = case.Case()
                num_colonne += 1
            num_colonne = 0
        elif i == plateau["nb_lignes"] :
            nb_joueur = int(liste_ligne[i])
        elif i > plateau["nb_lignes"] and i <= plateau["nb_lignes"]+nb_joueur :
            liste_valeur = liste_ligne[i].split(";")
            case.poser_joueur(plateau[(int(liste_valeur[1]), int(liste_valeur[2]))], liste_valeur[0])
        elif i > plateau["nb_lignes"] + 1 + nb_joueur :
            liste_valeur = liste_ligne[i].split(";")
            if len(liste_valeur) == 3 :
                case.poser_objet(plateau[(int(liste_valeur[1]), int(liste_valeur[2]))], int(liste_valeur[0]))
    return plateau

def est_sur_le_plateau(plateau, pos): 
    """_summary_

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si la position est sur le plateau, False sinon
    """    
    if pos[0] < 0 or pos[0]+1 > plateau["nb_lignes"] :
        return False
    elif pos[1] < 0 or pos[1]+1 > plateau["nb_colonnes"] :
        return False
    else :
        return True
    
def set_case(plateau, pos, une_case):
    """remplace la case qui se trouve en position pos du plateau par une_case

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
        une_case (dict): la nouvelle case
    """
    case=get_case(plateau,pos)
    case['mur']=une_case['mur']
    case['couleur']=une_case['couleur']
    case['objet']=une_case['objet']
    case["joueurs_presents"]=une_case["joueurs_presents"]
    return case

def enlever_joueur(plateau, joueur, pos):
    """enlève un joueur qui se trouve en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si l'opération s'est bien déroulée, False sinon
    """
    try :
        plateau[pos]["joueurs_presents"].remove(joueur)
        return True
    except :
        return False

def prendre_objet(plateau, pos):
    """Prend l'objet qui se trouve en position pos du plateau et retourne l'entier
        représentant cet objet. const.AUCUN indique qu'aucun objet se trouve sur case

    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        int: l'entier représentant l'objet qui se trouvait sur la case.
        const.AUCUN indique aucun objet
    """
    objet = plateau[pos]["objet"]
    plateau[pos]["objet"] = const.AUCUN
    return objet

def deplacer_joueur(plateau, joueur, pos, direction):
    """Déplace dans la direction indiquée un joueur se trouvant en position pos
        sur le plateau

    Args:
        plateau (dict): Le plateau considéré
        joueur (str): La lettre identifiant le joueur à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement

    Returns:
        tuple: un tuple contenant 4 informations
            - un bool indiquant si le déplacement a pu se faire ou non
            - un int valeur une des 3 valeurs suivantes:
                *  1 la case d'arrivée est de la couleur du joueur
                *  0 la case d'arrivée n'est pas peinte
                * -1 la case d'arrivée est d'une couleur autre que celle du joueur
            - un int indiquant si un objet se trouvait sur la case d'arrivée (dans ce
                cas l'objet est pris de la case d'arrivée)
            - une paire (lig,col) indiquant la position d'arrivée du joueur (None si
                le joueur n'a pas pu se déplacer)
    """
    deplacement = False
    couleur = None
    x = pos[0] + INC_DIRECTION[direction][0]
    y = pos[1] + INC_DIRECTION[direction][1]
    if not est_sur_le_plateau(plateau, (x, y)) :
        return (deplacement, 0, 0, None)
    elif case.est_mur(plateau[(x, y)]) :
        return (deplacement, 0, 0, None)
    elif not enlever_joueur(plateau, joueur, pos):
        return (deplacement, 0, 0, None)
    else :
        case.poser_joueur(plateau[(x, y)], joueur)
        deplacement = True
    if case.get_couleur(plateau[(x, y)]) == joueur :
        couleur = 1
    elif case.get_couleur(plateau[(x, y)]) == ' ' :
        couleur = 0
    else :
        couleur = -1
    objet = case.get_objet(plateau[(x, y)])
    case.prendre_objet(plateau[(x, y)])
    return (deplacement, couleur, objet, (x, y))

#-----------------------------
# fonctions d'observation du plateau
#-----------------------------

def surfaces_peintes(plateau, nb_joueurs):
    """retourne un dictionnaire indiquant le nombre de cases peintes pour chaque joueur.

    Args:
        plateau (dict): le plateau considéré
        nb_joueurs (int): le nombre de joueurs total participant à la partie

    Returns:
        dict: un dictionnaire dont les clées sont les identifiants joueurs et les
            valeurs le nombre de cases peintes par le joueur
    """
    surface = dict()
    liste_joueurs = 'ABCDEFGHIJQLMNOPQRSTUVWXYZ'
    for pos in plateau:
        if not isinstance(pos, str):
            la_case = get_case(plateau, pos)
            if la_case['couleur'] != ' ' and (la_case['couleur'].upper()) not in surface:
                surface[la_case['couleur'].upper()] = 1
            elif la_case['couleur'] != ' ':
                surface[la_case['couleur'].upper()] += 1
    for joueur in liste_joueurs[0:nb_joueurs]:
        if joueur not in surface:
            surface[joueur] = 0
    return surface
    
def directions_possibles(plateau,pos):
    """ retourne les directions vers où il est possible de se déplacer à partir
        de la position pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): un couple d'entiers (ligne,colonne) indiquant la position de départ
    
    Returns:
        dict: un dictionnaire dont les clés sont les directions possibles et les valeurs la couleur
              de la case d'arrivée si on prend cette direction
              à partir de pos
    """
    ensemble_direction_possible = dict()
    Nord = (pos[0] - 1, pos[1])
    Sud = (pos[0] + 1, pos[1])
    Est = (pos[0], pos[1] + 1)
    Ouest = (pos[0], pos[1] - 1)
    if est_sur_le_plateau(plateau, Nord):    
        if not case.est_mur(plateau[Nord]):
            ensemble_direction_possible['N'] = case.get_couleur(plateau[Nord])
    if est_sur_le_plateau(plateau, Sud):    
        if not case.est_mur(plateau[Sud]):
            ensemble_direction_possible['S'] = case.get_couleur(plateau[Sud])
    if est_sur_le_plateau(plateau, Est):
        if not case.est_mur(plateau[Est]):
            ensemble_direction_possible['E'] = case.get_couleur(plateau[Est])
    if est_sur_le_plateau(plateau, Ouest):
        if not case.est_mur(plateau[Ouest]):
            ensemble_direction_possible['O'] = case.get_couleur(plateau[Ouest])
    return ensemble_direction_possible

#directions_possibles(Plateau("4;6\n#  b# \n  A## \n##A   \n  Aa##\n2\nA;1;1\nB;1;0\n0"), (0, 2))
    
def nb_joueurs_direction(plateau, pos, direction, distance_max):
    """indique combien de joueurs se trouve à portée sans protection de mur.
        Attention! il faut compter les joueurs qui sont sur la case pos

    Args:
        plateau (dict): le plateau considéré
        pos (_type_): la position à partir de laquelle on fait le recherche
        direction (str): un caractère 'N','O','S','E' indiquant dans quelle direction on regarde
    Returns:
        int: le nombre de joueurs à portée de peinture (ou qui risque de nous peindre)
    """
    nombre=1
    cpt_joueur=0
    ligne_p, colonne_p = pos
    case_pos=case_distance=get_case(plateau,pos)
    if case_pos["joueurs_presents"]!=None :
             cpt_joueur+=1
    for i in range(distance_max):
        if direction == 'N':
            pos_distance = (ligne_p -nombre, colonne_p)
        elif direction == 'S':
            pos_distance = (ligne_p+nombre, colonne_p)
        elif direction == 'E':
            pos_distance = (ligne_p, colonne_p + nombre)
        elif direction == 'O':
            pos_distance = (ligne_p,colonne_p -nombre)
        A=est_sur_le_plateau(plateau,pos_distance)
        if A == False :
            return cpt_joueur
        case_distance=get_case(plateau,pos_distance)
        if case_distance["joueurs_presents"]!=None :
             cpt_joueur+=1
        if case_distance['mur']== True:
            return cpt_joueur
        nombre+=1
        
    return cpt_joueur
    
      
def peindre(plateau, pos, direction, couleur, reserve, distance_max, peindre_murs=False):
    """ Peint avec la couleur les cases du plateau à partir de la position pos dans
        la direction indiquée en s'arrêtant au premier mur ou au bord du plateau ou
        lorsque que la distance maximum a été atteinte.

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de int
        direction (str): un des caractères 'N','S','E','O' indiquant la direction de peinture
        couleur (str): une lettre indiquant l'idenfiant du joueur qui peint (couleur de la peinture)
        reserve (int): un entier indiquant la taille de la reserve de peinture du joueur
        distance_max (int): un entier indiquant la portée maximale du pistolet à peinture
        peindre_mur (bool): un booléen indiquant si on peint aussi les murs ou non

    Returns:
        dict: un dictionnaire avec 4 clés
                "cout": un entier indiquant le cout en unités de peinture de l'action
                "nb_repeintes": un entier indiquant le nombre de cases qui ont changé de couleur
                "nb_murs_repeints": un entier indiquant le nombre de murs qui ont changé de couleur
                "joueurs_touches": un ensemble (set) indiquant les joueurs touchés lors de l'action
    
    """
    la_direction = INC_DIRECTION[direction]
    res = {"cout": 0, "nb_repeintes": 0, "nb_murs_repeints": 0, "joueurs_touches": None}
    nb = 0
    joueur_touche = res["joueurs_touches"]
    if reserve < distance_max:
        distance_max = reserve
    while nb < distance_max and est_sur_le_plateau(plateau, pos):
        la_case = get_case(plateau, pos)
        la_couleur = la_case["couleur"]
        if la_case["joueurs_presents"] != None:
            if joueur_touche == None:
                joueur_touche = (la_case["joueurs_presents"])
            else:
                for joueur in la_case["joueurs_presents"]:
                    joueur_touche.add(joueur)
            res["joueurs_touches"] = joueur_touche
            case.peindre(la_case, couleur)
        if la_couleur == ' ' and not la_case["mur"] and reserve - 1 >= 0:
            reserve -= 1
            res["cout"] += 1
            res["nb_repeintes"] += 1
            case.peindre(la_case, couleur)
        elif la_couleur != couleur and not la_case["mur"] and reserve - 2 >= 0:
            reserve -= 2
            res['cout'] += 2
            res['nb_repeintes'] += 1
            case.peindre(la_case, couleur)
        elif la_couleur == couleur and reserve - 1 >= 0:
            reserve -= 1
            res["cout"] += 1
        elif la_case["mur"] and peindre_murs and la_couleur == ' ' and reserve - 1 >= 0:
            reserve -= 1
            res["cout"] += 1
            res["nb_repeintes"] += 1
            res["nb_murs_repeints"] += 1
            case.peindre(la_case, couleur.lower())
        elif la_case["mur"] and peindre_murs and la_couleur.upper() != couleur and reserve - 2 >= 0:
            reserve -= 2
            res['cout'] += 2
            res["nb_repeintes"] += 1
            res["nb_murs_repeints"] += 1
            case.peindre(la_case, couleur.lower())            
        else:
            break
        pos = (pos[0] + la_direction[0], pos[1] + la_direction[1])
        nb += 1
    return res