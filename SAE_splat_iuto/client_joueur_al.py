# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur

def info_objet(plan):
    """Cette fonction renvoie la liste des objets présent sur le plan ainsi que leurs position

    Args:
        plan (str): le plan du plateau comme comme indiqué dans le sujet

    Returns:
        dic: Un dictionnaire ayant pour clé l'identifiant des objets et pour valeurs leurs position
    """    
    liste_ligne = plan.split("\n")
    dico_objet = {}
    for i in range(1, len(liste_ligne)):
        try :
            pos_objet = liste_ligne[-i].split(";")
            if int(pos_objet[0]) not in dico_objet.keys():
                dico_objet[int(pos_objet[0])]= {(int(pos_objet[1]), int(pos_objet[2]))}
            else :
                dico_objet[int(pos_objet[0])].add((int(pos_objet[1]), int(pos_objet[2])))
        except :
            if len(liste_ligne) == 1:
                dico_objet["nb_objet"] = int(liste_ligne[-i])
            break
    return dico_objet

def objet_existe(plan, objet):
    dico_objet = info_objet(plan)
    try :
        ensemble_objet = dico_objet[objet]
        return True
    except :
        return False
def recherche_case(le_plateau, position_j, a_trouver):
    x, y = position_j
    case_depart = plateau.get_case(le_plateau, position_j)
    point_cardinaux = plateau.INC_DIRECTION
    if isinstance(a_trouver, str):
        case_act = case_depart
        while not case.est_mur(case_act) or case.get_couleur(case_act) != a_trouver:
            new_pos = ( x + point_cardinaux['N'][0], y + point_cardinaux['N'][1])



def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    # IA complètement aléatoire
    # ici il faudra décoder le plan, les joueur et les caractéristiques du jeu
    return random.choice("XNSEO") + random.choice("NSEO")
p2= "4;6\n#  b# \n  A## \n##A   \n  Aa##\n2\nA;1;1\nB;3;0\n0"
p1 = "12;12\n##A #  ##BB#\n #A  #   B# \n #A ##  ####\n #A         \n #A  #  # C#\n##A#DDDD##C \n# A# #    C#\n##A    ###CC\n##   #  #CCC\n# #   #  ##C\n #   # #   C\n ###     ###\n4\nB;0;9\nA;2;2\nD;5;5\nC;7;10\n1\n4;6;10"
#print(info_objet(p1))
#print(calque(plateau.Plateau(p1),(3,2)))
#mon_IA('A', "duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet", p2, "A;10;0;0;0;0;1;bidul\nB;10;1;0;0;1;1;truc")

#chercher_objet_plus_proche(p1, (3, 2))

if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,le_plateau,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,le_plateau,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")